--  first set
 create table int3 (       
     val      int         
 );      
 insert into int3 values ( 1 ) ;
 insert into int3 values ( 2 ) ;
 insert into int3 values ( 3 ) ;

--  second set
 create table int4 (       
     value      int         
 );      
 insert into int4 values ( 1 ) ;
 insert into int4 values ( 2 ) ;
 insert into int4 values ( 3 ) ;
 insert into int4 values ( 4 ) ;

