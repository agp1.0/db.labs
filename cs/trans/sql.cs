using System;
using System.IO;
using System.Data.Common;
class test{
  public static  void sql(bool cmt, DbConnection cnn, DbCommand cmd){
    int rc;
    for (int i = 0; i < 3; i++)
      using (DbTransaction tr =  cnn.BeginTransaction()){
         cmd.Transaction = tr;
         rc = cmd.ExecuteNonQuery();
         Console.WriteLine ("inserted/commit/isolation level: {0}/{1}/{2}"
                  , rc, cmt, tr.IsolationLevel);
         if (cmt)
           cmd.Transaction.Commit();
         else 
           cmd.Transaction.Rollback();
      }
  }
}


