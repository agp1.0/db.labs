--  some operator tests
 select 1 + 1, 1 - 1, 2 * 2, 8/4 ;
 select 'hello ' || ' world', substr( 'a baba golomaga', 3, 4);
 select 4 >> 1, 4 << 1,  2 | 1,  3 & 5;
 select 1 > 1, 1 >= 1,  2 < 3,  2 <= 3;
 select 1 = 1, 1 == 1,  2 != 3,  2 <> 2;
 select 1 is null, null is null, 1 is not null, null is not null;
 select 1 in (1,2,3), 10 in (1,2,3), 'baku' like '%ku';
 select 'hopa' like 'ho%', 'hopa' not like 'ho%';
 select  5 >= 2 and 5 =< 10, 5 between 2 and 10;
     


