  begin;
  select rowid, * from t ;  
  create index t_dtTmI on t(dtTm); 
  pragma  index_list (t);
  pragma  index_info (t_dtTmI);
  rollback;
  select 'rollback';
  pragma  index_list (t);
                                                          