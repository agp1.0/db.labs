select S.*, P.id, P.pName, t.total
  from (select s, p, sum(qtty) total from spj group by s,p ) t

           join S  on t.s = s.id
           join P  on t.p = p.id
;
