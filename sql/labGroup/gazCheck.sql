create table x (
  ID INTEGER primary key AUTOINCREMENT
, invoice REAL
, add1     real
, payment real
, duty    real
);


insert into x values (0.00 ,	8.39,	   0.00	  ,  224.68) ;
insert into x values (60.54,	0.00,	   121.08 ,	 216.29);
insert into x values (60.54,	0.00,	   0.00	 ,   276.83);
insert into x values (60.54,	0.00,	   63.25 ,	 216.30);
insert into x values (63.25,	0.00,	   0.00	 ,   219.01);
insert into x values (80.32,	0.00,	   90.43 ,	 155.76);
insert into x values (86.26,	4.17,	   43.13 ,	 165.87);
insert into x values (43.13,	0.00,	   0.00	  ,  118.57);
insert into x values (43.13,	10.43,   43.12	, 75.44  );
insert into x values (43.13,	-82.93,  64.69	, 65.00  );
insert into x values (64.69,	14.02	,  0.00	  ,  169.49 );
insert into x values (64.69,	26.09	,  129.38 ,	  90.78 );
insert into x values (64.69,	0.00	,  0.00	  ,  129.38 );
insert into x values (64.69,	0.00	,  0.00 	 ,64.69   );


select sum (invoice)+ sum(add1)                        total
         , sum(payment)                                totPayment
         ,   sum (invoice)+ sum(add1) -  sum(payment)  duty
from x;