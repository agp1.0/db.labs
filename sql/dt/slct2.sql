  select rowid, nt, dtTm, 'year'  from t    
               -- to select 1923 year
    where 
       cast(strftime('%Y', dtTm) AS INTEGER) == 1923
  ;  
  select rowid, nt, dtTm,   'january' from t    
       -- to select all the january
    where 
       cast(strftime('%m', dtTm) AS INTEGER) == 1
  ; 
  select rowid, nt, dtTm, 'friday'  from t    
    where 
       cast(strftime('%w', dtTm) AS INTEGER) == 5
  ; 
-- 0:sunday 1:monday 2:tuesday 3:wednesday 
-- 4:thursday 5:friday 6:saturday
  select rowid, nt, dtTm, '4th day of month'  from t 
    -- to select all the set day of month
    where 
       cast(strftime('%d', dtTm) AS INTEGER) ==  4
  ; 

