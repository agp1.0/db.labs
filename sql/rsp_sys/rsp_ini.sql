-- registration of student progress - rsp system
--
-- ���� ������������ ���������


 drop table if exists  Subject;
 create table Subject (       
      id       integer  primary key autoincrement
     ,code     varchar(4) not null   -- ���
     ,name     text       not null   -- ���������� �����������
     ,credit   int        not null   -- ���-�� ��������
 );      

 drop table if exists test;
 create table test (       
      id       integer  primary key autoincrement
     ,subj     integer               -- natural key
     ,semester int        not null   -- natural key
     ,activity integer    not null   -- ����� ��������
 );

 drop table if exists  activity;
 create table activity (       
      id       integer  primary key 
     ,name     varchar(8) not null   -- ��������
 );

 drop table if exists  class;
 create table class (       
      id        integer  primary key 
     ,subj      int
     ,semestr   int
     ,activite  int
     ,num      varchar(6) not null   -- �����
     ,name     varchar(24) not null   -- ��������
 );

 drop table if exists  group;
 create table group (       
       id       integer  primary key 
      ,creation int                -- ��� �������� ������
      ,note     varchar(24)
 );

 drop table if exists  subgroup;   -- ������ ������ � ��������
 create table subgroup (       
       id       integer  primary key 
      ,group    int                -- ������
      ,semestr  int                -- �������
      ,subgroup char(1)            -- ������� ��������� �� ������������
      ,note     varchar(24)
 );

 drop table if exists  compound;   -- ������ ������ � ��������
 create table compound (       
       id       integer  primary key 
      ,subgroup int                -- ������
      ,student  int                -- �������
 );


 drop table if exists  student;
 create table student (       
       id       integer  primary key 
      ,nameF    varchar(24)
      ,nameI    varchar(24)
      ,nameO    varchar(24)
      ,ordNum   int               -- ����� ��������� - ����� ������ ������� � �������
 );
