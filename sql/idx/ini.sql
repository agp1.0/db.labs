create table x (
  ObjectId    varchar (128),
  ObjectName  varchar (128),
  ObjectDesc  varchar (128),
  GpsDate     varchar (128),
  GpsTime     varchar (128),
  Longitude   double,
  Latitude    double,
  Height      int,
  Speed       int
);
select Longitude, ObjectId, ObjectName 
   from x 
   where Longitude > 30 and Longitude  < 40.1
;