using System;
using System.IO;
using System.Configuration;
using System.Data.Common;
class A {
   public static void Main() {
     try {// ����� �������� ��������� SQLite   ���
          // ������������� ����� ���� ������������,
          // � ������������ ������������ �����������
          // ������������ ����������� System.Data.SQLite.dll.
          // ��-������� �������� ������ ������� �� ���������
         var dataSet = ConfigurationManager.GetSection("system.data") 
             as System.Data.DataSet;
         dataSet.Tables[0].Rows.Add("SQLite Data Provider"
         , ".Net Framework Data Provider for SQLite"
         , "System.Data.SQLite"
         , "System.Data.SQLite.SQLiteFactory, System.Data.SQLite");
     }
     catch (System.Data.ConstraintException ex) { 
          Console.Error.WriteLine("Cann't work: '{0}'", ex.Message);
          Environment.Exit(1);
     }
     DbProviderFactory dbF 
            = DbProviderFactories.GetFactory("System.Data.SQLite");
     using(DbConnection cnn  = dbF.CreateConnection()){ 
        cnn.ConnectionString = "Data Source=.db";
        cnn.Open();
        // ������ ��������� new ������������ ����� CreateCommand
        DbCommand cmd   = cnn.CreateCommand();
        cmd.CommandText = 
@"create table if not exists Name11(id int, val text);
  insert into Name11 (id,val) values (13, 'Scott Tiger')";
        int rc=(int) cmd.ExecuteNonQuery();
        Console.WriteLine("I've inserted a single row: {0}", rc);
     }
   }
}