  BEGIN;
  CREATE INDEX t_dtTmI ON t(dtTm); 
  SELECT * FROM pragma_index_list('t');
  SELECT * FROM pragma_index_info('t_dtTmI');
  ROLLBACK;                                                          