create table t1 (
   a integer primary key,
   b int,
   c text,
   d int  generated always as (a*abs(b))        virtual,
   e text generated always as (substr(c,b,b+1)) stored
);
