using System;
using System.Data.Common;
using System.Data.SQLite;
class Program {
  static void Main() {
    using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
      using (DbConnection cnn = dbF.CreateConnection()) { // another version for connection
        cnn.ConnectionString = "Data Source=.db";
        cnn.Open();
        DbCommand cmd = cnn.CreateCommand();              
        cmd.CommandText = 
          @"insert into subject (code, name, test, credit) values (
             'ok10',
             'Алгоритмы и структуры данных',      
             't',      
               6)";
        Console.WriteLine(" {0} records inserted", cmd.ExecuteNonQuery());
      }
    }
  }
}
