 create table T (       
     id     int  ,       
     a      int  not null ,      
     b      int         
 );      
 insert into T           values ( 1, 10,  100) ;
 insert into T  (id, a)  values ( 2, 20 ) ;
 insert into T  ( a, id) values (30, 3  ) ;
 insert into T  ( id, b) values ( 4, 40 ) ;  -- it is error


select * from T;        -- you see nothing

-- additional initialization
.nullvalue null         

select * from T;        -- you see null
