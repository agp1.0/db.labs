select *
  from Teacher  T 
    join ( select teacher, count(*)   
             from Lecture             
             group by teacher          
         ) L
    on  T.id = L.teacher
 ; 