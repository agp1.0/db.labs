 drop table if exists State; 
 create table State (       
     id     integer  primary key autoincrement,       
     nm  Text
 );      
 insert into State ( nm) values ('ready') ;
 insert into State ( nm) values ('steady') ;
 insert into State ( nm) values ('go');  

 drop table if exists Color; 
 create table Color (       
     id     integer  primary key autoincrement,       
     nm  Text
 );      
 insert into Color ( nm) values ('red') ;
 insert into Color ( nm) values ('yellow') ;
 insert into Color ( nm) values ('green') ;  

 select * from State;
 select * from Color;