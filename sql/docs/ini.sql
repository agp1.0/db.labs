-- Create a test table with data
create table T1 (a int, b int, c int);
insert into  T1 values(1,0,0);
insert into  T1 values(2,0,1);
insert into  T1 values(3,1,0);
insert into  T1 values(4,1,1);
insert into  T1 values(5,null,0);
insert into  T1 values(6,null,1);
insert into  T1 values(7,null,null);
             
-- Check to  see what CASE does with NULLs in its test expressions
select a,    case when b<>0 then 1 else 0 end from T1;
select a+10, case when not b<>0 then 1 else 0 end from T1;
select a+20, case when b<>0 and c<>0 then 1 else 0 end from T1;
select a+30, case when not (b<>0 and c<>0) then 1 else 0 end from T1;
select a+40, case when b<>0 or c<>0 then 1 else 0 end from T1;
select a+50, case when not (b<>0 or c<>0) then 1 else 0 end from T1;
select a+60, case b when c then 1 else 0 end from T1;
select a+70, case c when b then 1 else 0 end from T1;

-- What happens when you multiple a NULL by zero?
select a+80, b*0 from T1;
select a+90, b*c from T1;

-- What happens to NULL for other operators?
select a+100, b+c from T1;

-- Test the treatment of aggregate operators
select count(*), count(b), sum(b), avg(b), min(b), max(b) from T1;

-- Check the behavior of NULLs in WHERE clauses
select a+110 from T1 where b<10;
select a+120 from T1 where not b>10;
select a+130 from T1 where b<10 OR c=1;
select a+140 from T1 where b<10 AND c=1;
select a+150 from T1 where not (b<10 AND c=1);
select a+160 from T1 where not (c=1 AND b<10);

-- Check the behavior of NULLs in a DISTINCT query
select distinct b from T1;

-- Check the behavior of NULLs in a UNION query
select b from T1 union select b from T1;

-- Create a new table with a unique column.  Check to see if NULLs are considered
-- to be distinct.
create table  T2 (a int, b int unique);
insert into   T2 values(1,1);
insert into   T2 values(2,null);
insert into   T2 values(3,null);
select * from T2;

drop table T1;
drop table T2;