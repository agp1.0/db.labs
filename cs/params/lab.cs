using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
class A{
  public static void Main(string[] args){
     using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
       using (DbConnection cnn=dbF.CreateConnection()){
         cnn.ConnectionString = "Data Source=.db";
         cnn.Open();
         using (DbCommand cmd = cnn.CreateCommand()){
              DbParameter par = cmd.CreateParameter();     // no Dispose() method
              par.DbType             = DbType.String;
              par.ParameterName               = "@nt";
//              par.Direction          = ParameterDirection.Input;
              cmd.CommandText = string.Format 
                             ("insert into  t(nt) values ({0})", par.ParameterName);
              cmd.Parameters.Add(par);
              test.sql(args.Length > 0? true: false, cnn, cmd, par);
         }
         Console.WriteLine("\nWe did the params+transaction demo! {0}", args.Length);
      }
    }
  }
}