--  �� �������� strict
--  3.38.5 2022-05-06 15:25:27 
--  78d9c993d404cdfaa7fdd2973fa1052e3da9f66215cff9c5540ebe55c407d9fe
create table t1 (a any) STRICT;
insert into  t1 values ( '000123') ;
create table t2 (a any);      
insert into  t2 values ( '000123') ;

select 'strict', typeof(a), quote(a) from t1
 union 
select 'non-strict', typeof(a), quote(a) from t2
;
