create table if not exists SP as 
  select S.id, S.sName
        , sum(  case when p.id = 'P1' then total else 0 end) 'Nut'
          , sum(  case when p.id = 'P2' then total else 0 end) 'Bolt'
            , sum(  case when p.id = 'P3' then total else 0 end) 'Screw Blue'
              , sum(  case when p.id = 'P4' then total else 0 end) 'Screw Red'
                , sum(  case when p.id = 'P5' then total else 0 end) 'Cam'
                  , sum(  case when p.id = 'P6' then total else 0 end) 'Cog'
    from (select s, p, sum(qtty) total from SPJ group by s, p ) T
             join S  on T.s = S.id
             join P  on T.p = P.id

    group by S.id, S.sName
;
select * from SP;
