create table t1 (
   a integer primary key,
   b int,
   c text,
   d int  as (a*abs(b))   ,
   e text as (substr(c,b,b+1)) stored
);
