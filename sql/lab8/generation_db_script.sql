Create table Students(

   id integer primary key autoincrement ,
   StudentName char(32) unique

);

Create table Subjects(

id integer primary key autoincrement,
SubjectName char(32) unique

);

Create table Exams(

id           integer primary key autoincrement,
StudentID    int, 	 
SubjectID    int,
DateField    char(16),
mark 		 int,

Constraint UniqueConstaint UNIQUE(StudentID, SubjectID, DateField)
,
Constraint StudentConstraint 
	FOREIGN KEY (StudentID)
	REFERENCES Students(id)
,
Constraint SubjectConstraint 
	FOREIGN KEY (SubjectID)
	REFERENCES Subjects(id)
		
);

Create view ExamsView as 
Select 
Exams.id as Id,
Subjects.SubjectName as SubjectName,
Students.StudentName as StudentName,
Exams.DateField as Date,
Exams.mark as Mark
From Exams 
Join Students On Students.id=Exams.StudentID
Join Subjects On Subjects.id=Exams.SubjectID;

 

