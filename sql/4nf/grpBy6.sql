EXPLAIN QUERY PLAN
select *
  from Teacher  T 
    join ( --  ������ ���� �������������, ������� ....
           select teacher, count(*) cnt  
             from Lecture             
             group by teacher          
             having cnt = ( -- ������� ������ � ���.���������
                            -- ���������� �������� ���������
                      select min (cnt) min
                        from  (  select teacher, count(*) cnt  
                                   from Lecture             
                                   group by teacher
                        )            
             )
    )  L
    on  T.id = L.teacher  --  �������� ��� �������������
 ;