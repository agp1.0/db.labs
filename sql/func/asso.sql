



WITH
 a
(id) AS

(SELECT * 
  FROM 
   (VALUES('1'),('2'),('3')) x(y)
)

select * from a;
,

/*

b(b_id) AS
(SELECT * FROM (VALUES('1'),('2'),('4')) x(y)), 
c(c_id) AS
(SELECT * FROM (VALUES('5'),('2'),('3')) x(y))
SELECT a_id, b_id, c_id  FROM (a LEFT JOIN b ON a_id=b_id) INNER JOIN c ON b_id=c_id
UNION ALL
SELECT '','',''
UNION ALL
SELECT a_id, b_id, c_id  FROM  a LEFT JOIN (b INNER JOIN c ON b_id=c_id) ON a_id=b_id;
*/