  create table foo(
     x , --text
     y  text
  );
  insert into foo 
   values
    (3.14159,   3.14159),
    ('3.14159', '3.14159'),
    (null, null),
    (x'314159', x'314159' )
  ;
.output .x
  select x, typeof (x), typeof(y)  from foo
  ;
