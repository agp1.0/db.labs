 drop table if exists T;  -- to delete table t

 create table T (       
     id     integer  primary key autoincrement,       
     a      int  not null ,      
     b      int  default (-100)       
 );      
-- insert into t           values ( 0, 10,  100) ;
 insert into T  (  a)  values (  20 ) ;        -- just b
 insert into T  (  a, b)  values (  30, 300) ;   

select * from T;        -- you see defaul values
select last_insert_rowid();
