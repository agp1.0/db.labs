.eqp on
create temp table PLnd 
  as select   P.id, P.Pname
   from P
   where P.city = 'London'
;
select P.id, P.Pname, sum(SPJ.qtty), 'tmp' 
  from  PLnd P join SPJ on P.id = SPJ.P
  group by P.id, P.Pname
;
