 select rowid, nt, dtTm, 'Greenwich time query'  from t  
   where 
      cast(strftime('%H', dtTm) AS INTEGER) between 12 and  15
 ; 
 select rowid, nt, dtTm, 'local time query'  from t    
   where 
      cast(strftime('%H', datetime(dtTm, 'localtime')) 
            AS INTEGER) between 12 and  15
 ; 
