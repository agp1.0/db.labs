using System;
using System.Data.Common;
using System.Data.SQLite;
using System.Text;

class Program {
  static void Main() {

    using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
      using (DbConnection cnn = dbF.CreateConnection()) { // another version for connection
        cnn.ConnectionString = "Data Source=.db";
        cnn.Open();
        DbCommand cmd = cnn.CreateCommand();              
        Console.InputEncoding = Encoding.GetEncoding(1251);
//        Console.InputEncoding = Encoding.GetEncoding(866);
        cmd.CommandText = Console.ReadLine();
        Console.WriteLine(" {0} records inserted", cmd.ExecuteNonQuery());
      }
    }
  }
}
