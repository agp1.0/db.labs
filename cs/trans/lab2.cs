using System;
using System.IO;
using System.Data.SQLite;
using System.Data.Common;

class A{

  public static void Main(string[] args){
     using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
       using (DbConnection cnn=dbF.CreateConnection()){
         cnn.ConnectionString = "Data Source=.db";
         cnn.Open();
         using (DbCommand cmd = cnn.CreateCommand()){
            cmd.CommandText = "insert into  t(nt) values ('transaction demo')";
            test.sql(args.Length > 0? true: false, cnn, cmd);
         }
         Console.WriteLine("\nWe did the transaction demo! {0}", args.Length);
      }
    }
  }
}