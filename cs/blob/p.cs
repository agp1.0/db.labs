using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.IO;
class Program {
  [STAThread]
  static void Main(string[] args) {
    bool add;
    add     = true;     // to insert some picture
    add     = false;    // to select picture
    long id = 4;
    string name;
    byte[] picture = File.ReadAllBytes("07.png");  // to read the test oicture
    SQLiteCommand command;
    using (SQLiteConnection cnn = 
                new SQLiteConnection("Data Source = test.db" )) {
       cnn.Open();
       if (add) { 
           using (command = new SQLiteCommand(
"INSERT INTO Pictures (picture, description, name) Values(@pict, 'blabla', @name)"
               , cnn)) {
                command.Parameters.Add(
                     "@pict"
                    , DbType.Binary
//                        , picture.Length, "picture"
                ).Value = picture;
                command.Parameters.Add(new SQLiteParameter("@name", "07.png"));
                 (long)command.ExecuteScalar();
            }
            Console.WriteLine(  "Picture is added");
       }
       else {
         using ( command = new SQLiteCommand(
               "select * from Pictures where description='blabla'", cnn)) {
           using (SQLiteDataReader reader = command.ExecuteReader()) {
             while (reader.Read())    {
                using (BinaryWriter writer 
                    = new BinaryWriter(new StreamWriter(".07.png").BaseStream)) {
                    int bufferSize = 4096;
                    byte[] outByte = new byte[bufferSize];
                    long startIndex = 0;
                    id = reader.GetBytes(1, startIndex, outByte, 0, bufferSize);
                    while (id == bufferSize) {
                        writer.Write(outByte, 0, bufferSize);
                        startIndex += bufferSize;
                        id = reader.GetBytes(1, startIndex, outByte, 0, bufferSize);
                    }
                    writer.Write(outByte, 0, (int)id - 1);
                }
                Console.WriteLine( "Picture is getted");
              }
            }  
         }
      }
    }
  }
}                
  	