 insert into  t     ( nt, dtTm)    values
 -- this two records have been inserted at another time of day
     ('right now 1'                ,  datetime('now', 'localtime'))    -- mistake
    ,('right now 2'                ,  CURRENT_TIMESTAMP) 
    ,('new Year',                  '2021-01-01 00:00:00' ) 
    ,('Unix Epoch',                '1970-01-01 00:00:00' ) 
    ,('Jesus Christ Era',          '0000-01-01 00:00:00' ) 
    ,('2000 year mistake',         '2000-01-01 00:00:00' ) 
    ,('E.F. Codd birthday',        '1923-08-19 00:00:00' ) 
    ,('V.M. Glushkov birthday',     '1923-08-23 00:00:00' ) 
    ,('Kenneth Thompson  birthday','1943-02-04 00:00:00' )
    ,('Alonzo Church  birthday',   '1903-06-14 00:00:00' )
    ,('Dennis Ritchie  birthday',  '1941-09-09 00:00:00' )
    ,('A.N. Kolmogorov  birthday',  '1903-04-24 00:00:00' )
    ,('O.Yu. Schmidt  birthday' ,  '1891-09-30 00:00:00' )
    ,('Sputnik  birthday' ,        '1957-10-04 00:00:00' )
    ,('M.V. Keldysh  birthday' ,   '1911-02-10 00:00:00' )
    ,('R.L. Bartini  birthday' ,   '1897-05-14 00:00:00' )
 ;

