using System;
using System.IO;
using System.Data.Common;

class test {
  public static  void sqlite(DbCommand cmd){
     using (DbDataReader  dr =  cmd.ExecuteReader()){
       Console.WriteLine ("#fields number: '{0}'", dr.FieldCount);
       if (dr.HasRows){
         Console.Write ("\n# names:");
         for (int i = 0; i < dr.FieldCount; i++)
             Console.Write (" {0};", dr.GetName(i));

         Console.WriteLine();
         while (dr.Read()){
             for (int i = 0; i < dr.FieldCount; i++) 
                 Console.Write(" {0};", dr.GetValue(i));
             Console.WriteLine();
         }
       }
     }
  }
}
