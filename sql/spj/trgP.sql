create trigger afterPIns after insert on P
begin
   insert into T  values (new.id, new.pName, 0);
end;
begin; 
  select 'insert into p', 'P13';
  insert into P values('P13','tstPart','tstColor', -1, 'tstCity');
  select * from T where id = 'P13';
rollback;
select * from T where id = 'P13';
drop trigger afterPIns;