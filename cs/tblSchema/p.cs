//#define PRAGMA
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Data;
using System.IO;

namespace SQLite_7 {
    class Program {
        [STAThread]
        static void Main(string[] args) {
            GetSchema();
        }
        static
        void GetSchema() {
            using (SQLiteConnection connection 
              = new SQLiteConnection(string.Format("Data Source={0};", "SPJ.db")))
            {
                connection.Open();
#if PRAGMA
                SQLiteCommand command 
                   = new SQLiteCommand("PRAGMA table_info('S');", connection);
                SQLiteDataReader dr = command.ExecuteReader();
                int k = dr.FieldCount;
          		for (int i = 0; i < dr.FieldCount; i++) {
                  	Console.Write("\n# {0}: {1} / {2}", dr.GetName(i)
                                                   , dr.GetFieldType(i)
                                                    , cmt(dr.GetName(i))
                                                   );
          		}
                while (dr.Read()) {
        			Console.WriteLine();
        			for (int i = 0; i < dr.FieldCount; i++) {
        				Console.Write(" '{0}'", dr.GetValue(i));
        			}
                }
#else
                SQLiteCommand command 
                  = new SQLiteCommand("select * from S;", connection);
                SQLiteDataReader dr = command.ExecuteReader();
// SQLiteDataReader dr = command.ExecuteReader(CommandBehavior.KeyInfo);
// SQLiteDataReader dr = command.ExecuteReader(CommandBehavior.SchemaOnly);
// SQLiteDataReader dr = command.ExecuteReader(CommandBehavior.Default);
                DataTable t  = dr.GetSchemaTable();
                string foo ;
                foreach (DataRow fld in t.Rows) {
                    Console.WriteLine(" ---- ");
                    foreach (DataColumn p in t.Columns) {
                        foo = slct(p.ColumnName);
                        if (foo != "") 
                          Console.WriteLine(" {0}: '{1}'", foo, fld[p]);
                    }
                }

#endif
                dr.Close();
            }
        }
        static string cmt( string fld){
          string rc = "none";
          switch (fld){
            case "cid":        rc = "������ ����";    break;
            case "name":       rc = "�������� ����";  break;
            case "type":       rc = "��� ����";       break;
            case "notnull":    rc = "���� not null?"; break;
            case "dflt_value": rc = "������������� ��������";     break;
            case "pk":         rc = "������ �� � ��������� ����"; break;
          }
          return rc;
        }
        static string slct( string fld) {
          string rc = "";
          switch (fld) {
            case "ColumnOrdinal":     rc = fld; break;
            case "ColumnName":        rc = fld; break;
            case "DataTypeName":      rc = fld; break;
            case "AllowDBNull":       rc = fld; break;
            case "DefaultValue":      rc = fld; break;
            case "IsKey":             rc = fld; break;
          }
          return rc;
        }
    }
}