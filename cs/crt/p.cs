using System;
using System.IO;
using System.Data.Common;
using System.Data.SQLite;

class Program {
  static void Main() {
    using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
      using (DbConnection cnn = dbF.CreateConnection()) { // another version for connection
        cnn.ConnectionString = "Data Source=test.db";
        cnn.Open();
        Console.WriteLine("connection was opened");
        DbCommand cmd = cnn.CreateCommand();              // another version for command
        cmd.CommandText = "select 1";
        Console.WriteLine(" 1 = {0}", cmd.ExecuteScalar());
        cmd.CommandText = 
          @"create table tbl (       
              id      int,         
              name    varchar(32), 
              surname varchar(32), 
              percent double,      
              salary  numeric(10,2))";
        cmd.ExecuteNonQuery();
        Console.WriteLine(" table tbl have been created");
        cmd.CommandText = 
          @"insert into tbl values (
             1,            
             'Scott',      
             'Tiger',      
             2.718281828,  
             300.21        
           )";
        Console.WriteLine(" {0} records inserted", cmd.ExecuteNonQuery());
      }
      Console.WriteLine("abstract version Ok");
    }
  }
}
