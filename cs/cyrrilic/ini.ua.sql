 drop table if exists  subject;

 create table subject (       
      id     integer  primary key autoincrement
     ,code    varchar(4)    
     ,name    varchar(32)    
     ,test    int
     ,credit  int
 );      


 insert into  subject (code, name, test, credit)
 values 
   ('ok6',  'Алгебра та геометрія', 'e', 12)
 , ('ok13', 'Обчислювальні методи', 'e', 18)
 ;