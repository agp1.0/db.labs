using System;
using System.IO;
using System.Data.Common;
using System.Data.SQLite;

class A{
   public static void Main(){
    using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
      using (DbConnection cnn=dbF.CreateConnection()){
        cnn.ConnectionString = "Data Source=.db";
        cnn.Open();
        using (DbCommand cmd = cnn.CreateCommand()){
           cmd.CommandText = "select rowid, * from t";
           test.sqlite(cmd);
        }
        Console.WriteLine("\nWe did the export!");
      }
    }
   }
}