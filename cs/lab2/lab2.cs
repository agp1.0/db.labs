using System;
using System.IO;
using System.Data.SQLite;
class A{
   public static void Main(){
      using (SQLiteConnection cnn = new SQLiteConnection ("Data Source=.db")){
         cnn.Open();
         SQLiteCommand cmd= new SQLiteCommand (
@"create table if not exists Name11(id int, val text);
  insert into Name11 (id,val) values (13, 'Scott Tiger')"
             ,   cnn);
          int rc=(int) cmd.ExecuteNonQuery();
          Console.WriteLine("I've inserted a single row: {0}", rc);
      }   // automatically used cnn.Close(); because of the using statement
   }
}