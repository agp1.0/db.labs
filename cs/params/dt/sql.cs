using System;
using System.IO;
using System.Text;
using System.Data.Common;
class test{
   public static string dtToA(DateTime value){
       return value.ToString("yyyy-MM-dd HH:mm:ss");
   }
   public static  void sql(DbConnection cnn, DbCommand cmd ){
     cmd.Parameters[0].Value = 
             string.Format("the utc time of Sputnik 1 start");
     DateTime
     t = new DateTime (1957, 10, 4, 19, 28, 34);
         // DateTimeKind.Utc    -- no difference ??
         // DateTimeKind.Local
//     cmd.Parameters[1].Value = dtToA(t);
     cmd.Parameters[1].Value = t;
     cmd.ExecuteNonQuery();

     cmd.Parameters[0].Value = "this is correct utc now";  
//     cmd.Parameters[1].Value =  dtToA(DateTime.UtcNow);  
     cmd.Parameters[1].Value =  DateTime.UtcNow;  
     cmd.ExecuteNonQuery();
  }
}