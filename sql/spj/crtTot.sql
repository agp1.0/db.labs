create table if not exists T as 
  select  P.id, P.pName, T.total
    from (select p, sum(qtty) total from SPJ group by p ) T
             join P  on T.p = P.id
;
select * from T;