create table Pictures (
  id          integer primary key autoincrement not null,
  picture     blob not null,
  name        text not null,
  description text not null
);
