using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
class A{
  public static void Main(){
     using (SQLiteFactory dbF = new SQLiteFactory()){      // new statement
       using (DbConnection cnn=dbF.CreateConnection()){
         cnn.ConnectionString = "Data Source=.db";
         cnn.Open();
         using (DbCommand cmd = cnn.CreateCommand()){
              DbParameter nt = cmd.CreateParameter();     // no Dispose() method
              nt.DbType             = DbType.String;
              nt.ParameterName               = "@nt";
              DbParameter dt = cmd.CreateParameter();     // no Dispose() method
              dt.DbType             = DbType.DateTime;
              dt.ParameterName               = "@dt";
              cmd.CommandText = string.Format 
                             ("insert into  t(nt, dtTm) values ({0}, {1})"
                                , nt.ParameterName, dt.ParameterName);
              cmd.Parameters.Add(nt);
              cmd.Parameters.Add(dt);
              test.sql( cnn, cmd);
         }
         Console.WriteLine("\nWe did the datetime demo!");
      }
    }
  }
}