pragma foreign_keys = on;
create table Subject (   -- �������� ���������
           id   integer primary key
         , name varchar(50)
);

insert into Subject values(1,'English');
insert into Subject values(2,'Geography');
insert into Subject values(3,'Phisics');
insert into Subject values(4,'Math');

create table Teacher (   -- ����� ��������������
        id   integer primary key
      , name varchar(50)
);
insert into Teacher values(1,'Klavdia Policarpovna');
insert into Teacher values(2,'Viktor Ivanovich');
insert into Teacher values(3,'Leonid Efimuch');

create table Room (  -- �������� ���������
     id      integer primary key
   , rnumber varchar(20)
);

insert into Room values(1,'104');
insert into Room values(2,'125');
insert into Room values(3,'402');
insert into Room values(4,'305');

create table Lecture (  -- ����� �������� ����� �������������
         id      integer primary key
       , subject integer
       , teacher integer
    , foreign key(subject) references Subject(Id)
    , foreign key(teacher) references Teacher(Id)
);
insert into Lecture values(1,1,1);
insert into Lecture values(2,1,2); -- ���. ������ ������ ��������
insert into Lecture values(3,2,1);
insert into Lecture values(4,3,1);
insert into Lecture values(5,3,2);
insert into Lecture values(6,3,3);

--delete from lecture where id = 2; --  ������� ���. �� ������.���������
create table Clas (     -- ����� ������ ������ � ���������
     id      integer primary key
   , subject integer
   , room    integer
   , foreign key(subject) references Subject(id)
   , foreign key(room)    references Room(id)
);

insert into Clas values(1,1,1);    -- ����. � 104
insert into Clas values(2,1,2);    -- ����. � 125
insert into Clas values(3,1,3);    -- ����. � 402
insert into Clas values(4,1,4);    -- ����. � 305
insert into Clas values(5,2,2);
insert into Clas values(6,3,3);
insert into Clas values(7,4,1);
insert into Clas values(8,4,4);

create view Acces as 
     SELECT S.id, S.name as SUBJECT, T.id, T.name as FIO, R.id, R.rnumber as Number 
  from Lecture as L, 
          Clas as C, 
            Subject as S, 
              Teacher as T, 
                    Room as R
  where     L.subject = C.subject
        and L.subject = S.id
        and L.teacher = T.id
        and C.room    = R.id
        order by fio asc
    ;