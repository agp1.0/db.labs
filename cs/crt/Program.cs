using System;
using System.IO;
using System.Data.SQLite;

class Program {
  static void Main() {
    using (SQLiteConnection cnn 
               = new SQLiteConnection("Data Source=test.db")) {
      cnn.Open();
      Console.WriteLine("connection was opened");
      SQLiteCommand cmd = new SQLiteCommand (cnn);
      cmd.CommandText = "select 1";
      Console.WriteLine(" 1 = {0}", cmd.ExecuteScalar());
    }
    Console.WriteLine("Ok");
  }
}
